# ACL howework Andrey Romanov

## Prerequisites
To run this lab root rights must be granted. You can simple became root or run `sudo` on each command:
```shell
$ sudo -i
```
or
```shell
$ sudo command
```
Also, acl filesystem extension must be enabled, you can check it
```shell
# tune2fs -l /dev/sdXY | grep "Default mount options"
```

## Run tests
1. Create environment
```shell
# make setup
``` 
2. Run tests 
```shell
# make test
``` 
3. Destroy environment
```shell
# make destroy
``` 
