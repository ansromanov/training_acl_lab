#!/usr/bin/env bash

USERS=$(cat ./*.usr) # parse files and extract usernames
PROJECTS=$(cat ./*.prj) # parse files and extract project names
PROJECTS_PERM=$(cat ./*.perm)
BASE_PATH="/u01"


for p in $PROJECTS_PERM; do
  echo "$p"
  p_name=$(echo $p | cut -d: -f1)
  p_perm=$(echo $p | cut -d: -f2 | cut -b-2)
  p_username=$(echo $p | cut -d: -f2 | cut -b3-)
  if [[ $p_perm  = "w_" ]]; then
    echo "Set write permissions to file for user $p_username in project $p_name"
  fi  
  if [[ $p_perm  = "r_" ]]; then
    echo "Set read permissions to file for user $p_username in project $p_name"
  fi  
done
