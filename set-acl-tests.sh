#!/usr/bin/env bash

# check root privileges
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

USERS=$(cat ./*.usr) # parse files and extract usernames
PROJECTS=$(cat ./*.prj) # parse files and extract project names
BASE_PATH="~/acl-test"


function createfile() {
  echo "File creation test"
  for p in $PROJECTS; do
    proj_path="$BASE_PATH/$p"
    echo -n "$p: "
    for u in $USERS; do
      echo -n "$u "
      sudo -u "$u" touch ""$proj_path"/"$p"_"$u".txt" 2>/dev/null
    done
    echo -e "\n"
    find "$BASE_PATH" -type f -name *"$p"*.txt -exec getfacl '{}' 2>/dev/null \;
  done
}


function exportdir() {
  ls -l "$BASE_PATH"
  for p in $PROJECTS; do
    proj_path="$BASE_PATH/$p"
    echo "Get ACL for project folder: $proj_path"
    getfacl "$proj_path" 2>/dev/null
  done
}


case $1 in
  exportdir)
    exportdir
    ;;
  createfile)
    createfile
    ;;
  *)
    echo "Available commands:"
    echo -e "\\t|--\\texportdir\\t\\tCreate project environment"
    exit 0
esac
