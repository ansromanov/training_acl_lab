#!/usr/bin/env bash

# check root privileges
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

USERS=$(cat ./*.usr) # parse files and extract usernames
PROJECTS=$(cat ./*.prj) # parse files and extract project names
PROJECTS_PERM=$(cat ./*.perm)
BASE_PATH="~/acl-test"

function addusers() {
  echo -n "Add user to system: "
  for u in $USERS; do
    echo -n "$u "
    useradd -b /home -m  "$u"
    echo "pass1" >> passwd "$u"
  done
  echo ""
}

function delusers() {
  echo -n "Del user in system: "
  for u in $USERS; do
    if [ $(getent passwd | cut -d: -f1 | grep "$u") ]; then
      echo -n "$u "
      userdel -fr "$u" 2>/dev/null
    fi 
  done
  echo ""
}

function setprojects() {
  echo  "Set project permisssions:"
  for p in $PROJECTS; do
    proj_path="$BASE_PATH/$p"
    chmod 777 "$BASE_PATH"
    mkdir -p "$proj_path"
    pwriter=""$p"_writer"
    preader=""$p"_reader"
    groupadd $pwriter
    groupadd $preader
    chmod 770 "$proj_path"
    setfacl -Rm g:"$pwriter":rwx "$proj_path"
    setfacl -Rm g:"$preader":rx "$proj_path"
    echo -e "$proj_path\t$pwriter,$preader"
  done
}

function delprojects() {
  for p in $PROJECTS; do
    proj_path="$BASE_PATH/$p"
    if [ -d "$proj_path" ]; then
      echo "Remove project folder: $proj_path"
      rm -fr "$proj_path"
    fi
    pwriter=""$p"_writer"
    preader=""$p"_reader"
    
    if [ $(getent group | cut -d: -f1 | grep "$pwriter") ]; then
      echo "Remove project security group: $pwriter"
      groupdel $pwriter
    fi
    if [ $(getent group | cut -d: -f1 | grep "$preader") ]; then
      echo "Remove project security group: $preader"
      groupdel $preader
    fi
  done
    if [ -d "$BASE_PATH" ]; then
      echo "Remove base projects folder: $BASE_PATH"
      rmdir "$BASE_PATH"
    fi
}

function setpermissions() {
  echo -n "Set user permissions: "
  output=""
  for p in $PROJECTS_PERM; do
    p_name=$(echo $p | cut -d: -f1)
    p_perm=$(echo $p | cut -d: -f2 | cut -b-2)
    p_username=$(echo $p | cut -d: -f2 | cut -b3-)
    pwriter=""$p_name"_writer"
    preader=""$p_name"_reader"

    if [[ $p_perm  = "w_" ]]; then
      output+="$p_username: $pwriter\n"
      usermod -aG $pwriter $p_username  
    fi  
    if [[ $p_perm  = "r_" ]]; then
      output+="$p_username: $preader\n"
      usermod -aG $preader $p_username  
    fi  
  done
  echo -e $output | sort
}

case $1 in
  addusers)
    addusers
    ;;
  delusers)
    delusers
    ;;
  setprojects)
    setprojects
    ;;
  delprojects)
    delprojects
    ;;
  setpermissions)
    setpermissions
    ;;
  *)
    echo "Available commands:"
    echo -e "\\t|--\\taddusers:\\t\\tCreate project environment"
    echo -e "\\t|--\\tdelusers:\\t\\tCreate project environment"
    echo -e "\\t|--\\tsetprojects:\\t\\tCreate project environment"
    echo -e "\\t|--\\tdelprojects:\\t\\tCreate project environment"
    exit 0
esac
