# Cosmetics
YELLOW := "\e[1;33m"
NC := "\e[0m"

# Shell Functions
INFO := @bash -c '\
	printf $(YELLOW); \
	echo "=> $$1"; \
	printf $(NC)' SOME_VALUE

# Variable
set-acl.sh=sudo ./set-acl.sh

.PHONY: setup destroy test ci

setup:
	$(INFO) "Setting up projects"
	@./set-acl.sh addusers
	@./set-acl.sh setprojects
	@./set-acl.sh setpermissions

destroy:
	$(INFO) "*** Destroying projects ***"
	@./set-acl.sh delprojects
	@./set-acl.sh delusers

test:
	$(INFO) "*** Running tests ***"
	@./set-acl-tests.sh exportdir
	@./set-acl-tests.sh createfile

ci: setup test destroy
